#include<bits/stdc++.h>
using namespace std;

void max_pool(vector<vector<int>> &output,int out_height,int out_width,
                 int filter_y,int filter_x,int stride)
{
   for (size_t y = 0; y < out_height; ++y) {
     for (size_t x = 0; x < out_width; ++x) {
        for (size_t i = 0; i < filter_y; ++i) {
            for (size_t j = 0; j < filter_x; ++j) {
                    int value = in[y * filter_y + i][x * filter_x + j];
                    out[y][x] = max(out[y][x], value);

            }
        }
    }
    }
}

vector<vector<int>> quantization_float_to_int(vector<vector<float>> input)
{
    vector<vector<int>> result;
    //max value 10--> 4 bits
    int no_of_bits=4;
    int size=input.size();
    for(int i=0;i<size;i++){
            for(int j=0;j<size;j++){
                result[i].push_back((int)(input[i]*pow(2,no_of_bits)));
                //cout<<result[i]<<endl;}
        }
    return result;
}

vector<vector<float>> quantization_int_to_float(vector<vector<int>> output)
{
    vector<vector<float>> result;
    //max value 10--> 4 bits
    int no_of_bits=4;
    int size=output.size();
    for(int i=0;i<size;i++){
            for(int j=0;j<size;j++){
                result[i].push_back((float)(output[i]/pow(2,no_of_bits)));
                //cout<<result[i]<<endl;}
        }
    return result;
}

int main()
{
    vector<float> image={2.5,1.5};
	vector<int> int_values;
	int_values=quantization_float_to_int(image);
	for(int i=0;i<int_values.size();i++)
	{
	    cout<<int_values[i]<<endl;
	}
}

