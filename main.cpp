#include <bits/stdc++.h>
#include <tensorflow/c/c_api.h>
#include "cppflow/cppflow.h"
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#define indexToOffset(x, y, channel, heightOffset, widthOffset) ((channel * H * W) + (heightOffset + y) * W + widthOffset + x)

using namespace std;

void convolution(vector<double> &image,vector<double> &filter,vector<double> &output,int image_height,int image_width,int padded_height,int padded_width,int im_channel,int out_height,int out_width,int out_channel,
                 int ksize,int batch, int stride)
{
	int image_index,kernel_index,output_index,in_h,in_w;
	for (int b = 0; b < batch; b++)
	{
		for (int oc = 0; oc < out_channel; oc++)
		{
			for (int oh = 0; oh < out_height; oh++)
			{
				for (int ow = 0; ow < out_width; ow++)
				{
					int sum = 0;
					for (int c = 0; c < im_channel; c++)
					{
						for (int kh = 0; kh < ksize; kh++)
						{
							for (int kw = 0; kw < ksize; kw++)
							{
							 	in_h = (oh * stride) + kh;
								in_w = (ow * stride) + kw;
								image_index = ((b * im_channel * padded_height
										* padded_width) + (c * padded_height
										* padded_width) + in_h * image_width
										+ in_w);

								kernel_index = ((oc * im_channel * ksize
										* ksize)  + (c * ksize * ksize) +  kh
										* ksize + kw );

								sum += image[image_index]
										* filter[kernel_index];

							}
						}
					}
					output_index =((b*out_channel*out_height*out_width)+(oc*out_height*out_width)+oh*out_width+ow);
					output[output_index] = sum;

				}
			}
		}
	}

}

__global__ void cuda_conv(double *image,double *filter, double *output,int image_height,int image_width,int padded_height,int padded_width,int im_channel,int out_height,int out_width,int out_channel,
                 int ksize,int batch, int stride){


    int N=ksize;
    int sum=0;
    int oc=1,b=1,c=1;
    for(int i=0;i<N;i++){
      for(int j=0;j<N;j++){
      int in_h = (threadIdx.y * stride) + j;
      int in_w = (threadIdx.x * stride) + i;

      int image_index = ((b * im_channel * padded_height
                      * padded_width) + (c * padded_height
                      * padded_width) + in_h * image_width
                      + in_w);

      int kernel_index = ((oc * im_channel * ksize
                      * ksize)  + (c * ksize * ksize) +  j
                      * ksize + i );

      sum += image[image_index]
                      * filter[kernel_index];
      }
    }
    int output_index =((b*out_channel*out_height*out_width)+(oc*out_height*out_width)+threadIdx.y*out_width+threadIdx.x);
		output[output_index] = sum;

}

void max_pool(vector<vector<int>> &output,int out_height,int out_width,
                 int filter_y,int filter_x,int stride)
{
   for (size_t y = 0; y < out_height; ++y) {
     for (size_t x = 0; x < out_width; ++x) {
        for (size_t i = 0; i < filter_y; ++i) {
            for (size_t j = 0; j < filter_x; ++j) {
                    float value = in[y * filter_y + i][x * filter_x + j];
                    out[y][x] = max(out[y][x], value);

            }
        }
    }
}
}

__global__ void cudamaxpool(double* gOutImage, double* gImage, int c, int h, int w, int fw, int fh) {

  int blockWidth = blockDim.x;
  int blockHeight = blockDim.y;
  int blockWidthOffset = blockIdx.y;
  int blockHeightOffset = blockIdx.z;

  int widthOffset = pixel_x(blockWidth, blockWidthOffset, threadIdx.x);
  int heightOffset = pixel_y(blockHeight, blockHeightOffset, threadIdx.y);
  int channel = blockIdx.x;

  if (widthOffset < 0 || heightOffset < 0 || widthOffset >= w || heightOffset >= h) {
    return;
  }

  double maxValue = gImage[indexToOffset(0, 0, channel, heightOffset, widthOffset)];
  for (int x = -fw/2; x <= fw/2; x++) {
    for (int y = -fh/2; y <= fh/2; y++) {
      double value = 0.0;
      if ((widthOffset + x) >= 0 && (widthOffset + x) < w && (heightOffset + y) >= 0 && (heightOffset + y) < h) {
        value = gImage[indexToOffset(x, y, channel, heightOffset, widthOffset)];
      }
      if (value > maxValue) {
        maxValue = value;
      }
    }
  }

  gOutImage[indexToOffset(0, 0, channel, heightOffset, widthOffset)] = maxValue;
}


vecotr<double>get_reference_conv(vector<double> &image,vector<double> &filter,vector<double> &actual,int image_height,int image_width,int padded_height,int padded_width,int im_channel,
                   int out_height,int out_width,int out_channel,int ksize,int batch,int stride){

    vector<int> image_shape={1,3,3,1};
    vector<int> filter_shape={2,2,1,1};

    auto tf_image_shape = vector<int64_t>(image_shape.begin(), image_shape.end());
    auto tf_filter_shape = vector<int64_t>(filter_shape.begin(), filter_shape.end());

   	auto tf_image_tensor = cppflow::tensor(image,tf_image_shape);
    auto tf_filter_tensor = cppflow::tensor(filter,tf_filter_shape);
	auto output_tensor = cppflow::conv2_d(tf_image_tensor,tf_filter_tensor,{1,1,1,1},"VALID",
                                    {},{1,1,1,1});

    vector<double>expected=output_tensor.get_data<double>();
    return expected;
}

vecotr<double>get_reference_maxpool(vector<double> &image,vector<double> &filter,vector<double> &actual,int image_height,int image_width,int padded_height,int padded_width,int im_channel,
                   int out_height,int out_width,int out_channel,int ksize,int batch,int stride){

    vector<int> image_shape={1,3,3,1};
    vector<int> filter_shape={2,2,1,1};

    auto tf_image_shape = vector<int64_t>(image_shape.begin(), image_shape.end());
    auto tf_filter_shape = vector<int64_t>(filter_shape.begin(), filter_shape.end());

   	auto tf_image_tensor = cppflow::tensor(image,tf_image_shape);
    auto tf_filter_tensor = cppflow::tensor(filter,tf_filter_shape);
	auto output_tensor = cppflow::max_pool(tf_image_tensor,tf_filter_tensor,{1,1,1,1},"VALID",
                                    {},{1,1,1,1});

    vector<double>expected=output_tensor.get_data<double>();
    return expected;
}

//compare reference and conv pure cpp
void conv_test1(vector<double> &image,vector<double> &filter,vector<double> &actual,int image_height,int image_width,int padded_height,int padded_width,int im_channel,
                   int out_height,int out_width,int out_channel,int ksize,int batch,int stride)
{
    vector<double>expected=get_reference_conv(image,filter,actual,image_height,image_width,padded_height,padded_width,im_channel,out_height,out_width,out_channel,ksize,batch,stride);
	convolution(image,filter,actual,image_height,image_width,padded_height,padded_width,im_channel,out_height,out_width,out_channel,ksize,batch,stride);

	for(int i=0;i<expected.size();i++){
		if(expected[i]!=actual[i]){
			cout<<"FAILED"<<endl;
			return;}
	}

	cout<<"\nPASSED!"<<endl;
}

//compare reference and conv cuda
void conv_test2(vector<double> &image,vector<double> &filter,vector<double> &actual,int image_height,int image_width,int padded_height,int padded_width,int im_channel,
                   int out_height,int out_width,int out_channel,int ksize,int batch,int stride){

  double *d_img;
  double *d_fil;
  double *d_out;

  int image_size=9*sizeof(double),filter_size=4*sizeof(double),output_size=4*sizeof(double);

  cudaMalloc((void **)&d_img, image_size);
  cudaMalloc((void **)&d_fil, filter_size);
  cudaMalloc((void **)&d_out, output_size);


  cudaMemcpy(d_img, image, image_size, cudaMemcpyHostToDevice);
  cudaMemcpy(d_fil, filter, filter_size, cudaMemcpyHostToDevice);
  cudaMemcpy(d_out, output, output_size, cudaMemcpyHostToDevice);
	dim3 thread_dim(out_height,out_width);
  //function invocation
  cuda_conv<<<1,thread_dim>>>(d_img,d_fil,d_out,image_height,image_width,padded_height,padded_width,im_channel,out_height,out_width,out_channel,
                 ksize,batch,stride);

  cudaMemcpy(output, d_out, output_size, cudaMemcpyDeviceToHost);

  vector<double> reference = get_reference_conv(image,filter,output,image_height,image_width,padded_height,padded_width,im_channel,out_height,out_width,out_channel,ksize,batch,stride);
  for(int i=0;i<output_size;i++)
  {
      if(output[i]!=reference[i]){cout<<"FAILED";return;}
  }
  cout<<"PASSED"<<endl;

}

//compare reference and maxpool pure cpp
void maxpool_test1(vector<vector<int>> &output,int out_height,int out_width,
                 int filter_y,int filter_x,int stride){
    vector<double>expected=get_reference_maxpool(image,filter,actual,image_height,image_width,padded_height,padded_width,im_channel,out_height,out_width,out_channel,ksize,batch,stride);
	max_pool(output,out_height,out_width,out_channel,filter_x,filter_y);

	for(int i=0;i<expected.size();i++){
		if(expected[i]!=actual[i]){
			cout<<"FAILED"<<endl;
			return;}
	}

	cout<<"\nPASSED!"<<endl;

}

//compare reference and cuda maxpool
void maxpool_test2(vector<double> &image,vector<double> &filter,vector<double> &actual,int image_height,int image_width,int padded_height,int padded_width,int im_channel,
                   int out_height,int out_width,int out_channel,int ksize,int batch,int stride){

  double *d_img;
  double *d_fil;
  double *d_out;

  int image_size=9*sizeof(double),filter_size=4*sizeof(double),output_size=4*sizeof(double);

  cudaMalloc((void **)&d_img, image_size);
  cudaMalloc((void **)&d_fil, filter_size);
  cudaMalloc((void **)&d_out, output_size);


  cudaMemcpy(d_img, image, image_size, cudaMemcpyHostToDevice);
  cudaMemcpy(d_fil, filter, filter_size, cudaMemcpyHostToDevice);
  cudaMemcpy(d_out, output, output_size, cudaMemcpyHostToDevice);
	dim3 thread_dim(out_height,out_width);
  //function invocation
  cuda_maxpool<<<1,thread_dim>>>(d_img,d_fil,d_out,im_channel,out_height,out_width,out_channel,
                 kheight,kwidth);

  cudaMemcpy(output, d_out, output_size, cudaMemcpyDeviceToHost);

  vector<double> reference = get_reference_maxpool(image,filter,output,image_height,image_width,padded_height,padded_width,im_channel,out_height,out_width,out_channel,ksize,batch,stride);
  for(int i=0;i<output_size;i++)
  {
      if(output[i]!=reference[i]){cout<<"FAILED";return;}
  }
  cout<<"PASSED"<<endl;

}
int main()
{
	double image[]={1,2,3,4,5,6,7,8,9};
	double filter[]={1,1,1,1};
	double output[4];
	int image_height=3;
	int image_width=3;
	int padded_height=0;
	int padded_width =0;
	int im_channel=1;
	int out_height=2;
	int out_width=2;
	int out_channel=1;
	int ksize=2;
	int batch=1;
	int stride=1;

	//covolution test
    conv_test1(image,filter,output,image_height,image_width,padded_height,padded_width,im_channel,out_height,out_width,out_channel,ksize,batch,stride);
    conv_test2(image,filter,output,image_height,image_width,padded_height,padded_width,im_channel,out_height,out_width,out_channel,ksize,batch,stride);

    vector<vector<int>> max_pool_output();
	out_height=5;
	out_width=5;
	out_channel=1;
	int filter_x=3;
	int filter_y=3;
	batch=1;
	stride=2;

	//maxpool test
    maxpool_test1(max_pool_output,out_height,out_width,out_channel,filter_x,filter_y,stride);
    maxpool_test2(image,output,out_height,out_width,out_channel,filter_x,filter_y,stride);

  return 0;
}
