#include<bits/stdc++.h>
using namespace std;

void convolution(vector<int> &image,vector<int> &filter,vector<int> &output,int image_height,int image_width,int padded_height,int padded_width,int im_channel,int out_height,int out_width,int out_channel,
                 int ksize,int batch, int stride)
{
	int image_index,kernel_index,output_index,in_h,in_w;
	for (int b = 0; b < batch; b++)
	{
		for (int oc = 0; oc < out_channel; oc++)
		{
			for (int oh = 0; oh < out_height; oh++)
			{
				for (int ow = 0; ow < out_width; ow++)
				{
					int sum = 0;
					for (int c = 0; c < im_channel; c++)
					{
						for (int kh = 0; kh < ksize; kh++)
						{
							for (int kw = 0; kw < ksize; kw++)
							{
							 	in_h = (oh * stride) + kh;
								in_w = (ow * stride) + kw;
								image_index = ((b * im_channel * padded_height
										* padded_width) + (c * padded_height
										* padded_width) + in_h * image_width
										+ in_w);

								kernel_index = ((oc * im_channel * ksize
										* ksize)  + (c * ksize * ksize) +  kh
										* ksize + kw );

								sum += image[image_index]
										* filter[kernel_index];

							}
						}
					}
					output_index =((b*out_channel*out_height*out_width)+(oc*out_height*out_width)+oh*out_width+ow);
					output[output_index] = sum;

				}
			}
		}
	}

}

vector<int> quantization_float_to_int(vector<float> input)
{
    vector<int> result;
    //max value 10--> 4 bits
    int no_of_bits = 4;
    int size = input.size();
    for(int i = 0;i < size; i++){
                result.push_back((int)(input[i]*pow(2,no_of_bits)));
        }
    return result;
}

vector<float> quantization_int_to_float(vector<int> output)
{
    vector<float> result;
    //max value 10--> 4 bits
    int no_of_bits = 4;
    int size = output.size();
    for(int i = 0;i < size; i++){
                result.push_back((float)(output[i]/pow(2,2*no_of_bits)));
        }
    return result;
}
